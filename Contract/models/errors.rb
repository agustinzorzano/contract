class ContractsError < RuntimeError
end

class InvalidContractAction < ContractsError
end

class InvalidContractInfo < ContractsError
end

class InvalidAmendment < ContractsError
end
