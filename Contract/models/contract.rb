require_relative './errors'

class Contract
  attr_reader :date, :client, :info

  def initialize(date, client, info)
    @date = date
    @client = client
    @info = info
    @confirmed = false
    @amendments = []
  end

  def add_amendment(amendment)
    raise InvalidContractAction unless @confirmed

    @amendments << amendment
  end

  def modify_info(info)
    raise InvalidContractAction if @confirmed

    info.validate
    @info = info
  end

  def confirm
    @confirmed = true
  end

  def original
    @info.copy
  end

  def current
    at_amendment(@amendments.size)
  end

  def at_amendment(number)
    info = original
    @amendments.each_with_index do |amendment, index|
      info = amendment.apply(info) if index < number
    end
    info
  end
end
