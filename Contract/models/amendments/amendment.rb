require 'date'

class Amendment
  attr_reader :date

  def initialize
    @date = Date.today
  end

  def apply(contract_info)
    apply_amendment(contract_info.copy)
  end

  protected

  def apply_amendment(_contract_info)
    raise NotImplementedError
  end
end
