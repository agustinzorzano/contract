require_relative 'amendment'
require_relative '../errors'

class AmendmentRepetitions < Amendment
  def initialize(repetitions)
    super()
    @repetitions = repetitions
    validate
  end

  protected

  def apply_amendment(contract_info)
    contract_info.repetitions = @repetitions
    contract_info
  end

  private

  def validate
    raise InvalidAmendment unless @repetitions > 0
  end
end
