require_relative 'amendment'
require_relative '../errors'

class AmendmentFrequency < Amendment
  def initialize(frequency)
    super()
    @frequency = frequency
    validate
  end

  protected

  def apply_amendment(contract_info)
    contract_info.frequency = @frequency
    contract_info
  end

  private

  def validate
    raise InvalidAmendment unless @frequency > 0
  end
end
