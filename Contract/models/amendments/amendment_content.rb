require_relative 'amendment'
require_relative '../errors'

class AmendmentContent < Amendment
  def initialize(content)
    super()
    @content = content
    validate
  end

  protected

  def apply_amendment(contract_info)
    contract_info.content = @content
    contract_info
  end

  private

  def validate
    raise InvalidAmendment if @content.empty?
  end
end
