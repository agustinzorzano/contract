require_relative 'amendment'
require_relative '../errors'

class AmendmentAmount < Amendment
  def initialize(amount)
    super()
    @amount = amount
    validate
  end

  protected

  def apply_amendment(contract_info)
    contract_info.amount = @amount
    contract_info
  end

  private

  def validate
    raise InvalidAmendment unless @amount > 0
  end
end
