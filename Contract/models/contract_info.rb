require_relative './errors'

class ContractInfo
  attr_accessor :amount, :content, :repetitions, :frequency, :signals

  def initialize(amount, content, repetitions, frequency, signals)
    @amount = amount
    @content = content
    @repetitions = repetitions
    @frequency = frequency
    @signals = signals

    validate
  end

  def copy
    ContractInfo.new(@amount, @content, @repetitions, @frequency, @signals)
  end

  def validate
    raise InvalidContractInfo if @content.empty?

    raise InvalidContractInfo if @signals.empty?

    raise InvalidContractInfo unless @amount > 0 &&
                                     @repetitions > 0 &&
                                     @frequency > 0
  end
end
