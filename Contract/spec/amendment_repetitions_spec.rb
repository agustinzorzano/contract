require 'rspec'
require_relative '../models/amendments/amendment_repetitions'
require_relative '../models/contract_info'
require_relative '../models/errors'

describe 'AmendmentRepetitions' do
  let(:contract_info) { ContractInfo.new(10_000, ['Harry Potter'], 3, 7, ['channel1']) }
  let(:amendment) { AmendmentRepetitions.new(10) }

  it 'applies correctly to contract info' do
    expect(amendment.apply(contract_info).repetitions).to eq(10)
  end

  it 'it is invalid if repetitions is negative' do
    expect { AmendmentRepetitions.new(-2) }.to raise_error(InvalidAmendment)
  end
end
