require 'rspec'
require 'date'
require_relative '../models/contract_info'
require_relative '../models/errors'

describe 'ContractInfo' do
  let(:info) { ContractInfo.new(10_000, ['Harry Potter'], 3, 7, ['channel1']) }
  let(:copy) { info.copy }

  describe 'info data' do
    it 'info amount is correct' do
      expect(info.amount).to eq(10_000)
    end

    it 'info conent is correct' do
      expect(info.content[0]).to eq('Harry Potter')
    end

    it 'info repetitions is correct' do
      expect(info.repetitions).to eq(3)
    end

    it 'info frequency is correct' do
      expect(info.frequency).to eq(7)
    end

    it 'info signals is correct' do
      expect(info.signals[0]).to eq('channel1')
    end
  end

  describe 'copy' do
    it 'copy amount is correct' do
      expect(copy.amount).to eq(10_000)
    end

    it 'copy conent is correct' do
      expect(copy.content[0]).to eq('Harry Potter')
    end

    it 'copy repetitions is correct' do
      expect(copy.repetitions).to eq(3)
    end

    it 'copy frequency is correct' do
      expect(copy.frequency).to eq(7)
    end

    it 'copy signals is correct' do
      expect(copy.signals[0]).to eq('channel1')
    end
  end

  describe 'validations' do
    it 'is invalid if amount is negative' do
      info.amount = -54
      expect { info.validate }.to raise_error(InvalidContractInfo)
    end

    it 'is invalid if repetitions is negative' do
      info.repetitions = -54
      expect { info.validate }.to raise_error(InvalidContractInfo)
    end

    it 'is invalid if frequency is negative' do
      info.frequency = -54
      expect { info.validate }.to raise_error(InvalidContractInfo)
    end

    it 'is invalid if content is empty' do
      info.content = []
      expect { info.validate }.to raise_error(InvalidContractInfo)
    end

    it 'is invalid if signals is empty' do
      info.signals = []
      expect { info.validate }.to raise_error(InvalidContractInfo)
    end
  end
end
