require 'rspec'
require_relative '../models/amendments/amendment_frequency'
require_relative '../models/contract_info'
require_relative '../models/errors'

describe 'AmendmentFrequency' do
  let(:contract_info) { ContractInfo.new(10_000, ['Harry Potter'], 3, 7, ['channel1']) }
  let(:amendment) { AmendmentFrequency.new(2) }

  it 'applies correctly to contract info' do
    expect(amendment.apply(contract_info).frequency).to eq(2)
  end

  it 'it is invalid if frequency is negative' do
    expect { AmendmentFrequency.new(-1) }.to raise_error(InvalidAmendment)
  end
end
