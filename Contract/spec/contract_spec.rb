require 'rspec'
require 'date'
require_relative '../models/contract'
require_relative '../models/errors'

describe 'Contract' do
  let(:date) { DateTime.parse('2019-05-10') }
  let(:contract_info) { ContractInfo.new(10_000, ['Harry Potter'], 3, 7, ['channel1']) }
  let(:contract) { Contract.new(date, 'pepe', contract_info) }
  let(:amendment_repetitions_1) { AmendmentRepetitions.new(10) }
  let(:amendment_repetitions_2) { AmendmentRepetitions.new(20) }
  let(:amendment_amount_1) { AmendmentAmount.new(20_000) }
  let(:amendment_amount_2) { AmendmentAmount.new(5_000) }

  it 'contract date is correct' do
    expect(contract.date).to eq(date)
  end

  it 'contract client is correct' do
    expect(contract.client).to eq('pepe')
  end

  it 'contract info is correct' do
    expect(contract.info).to eq(contract_info)
  end

  it 'unconfirmed contract can be modified' do
    new_info = contract_info.copy
    new_info.amount = 20_000
    contract.modify_info(new_info)
    expect(contract.current.amount).to eq(20_000)
  end

  it 'contract can not be modified with invalid info' do
    new_info = contract_info.copy
    new_info.amount = -20_000
    expect { contract.modify_info(new_info) }.to raise_error(InvalidContractInfo)
  end

  it 'confirmed contract can not be modified' do
    contract.confirm
    new_info = contract_info.copy
    new_info.amount = 20_000
    expect { contract.modify_info(new_info) }.to raise_error(InvalidContractAction)
  end

  it 'apply amendment to unconfirmed contract' do
    expect { contract.add_amendment(amendment_repetitions_1) }.to raise_error(InvalidContractAction)
  end

  it 'apply 1 repetitions amendment to confirmed contract' do
    contract.confirm
    contract.add_amendment(amendment_repetitions_1)
    expect(contract.current.repetitions).to eq(10)
  end

  it 'apply 1 amount amendment to confirmed contract' do
    contract.confirm
    contract.add_amendment(amendment_amount_1)
    expect(contract.current.amount).to eq(20_000)
  end

  it 'apply 2 amount amendment2 to confirmed contract' do
    contract.confirm
    contract.add_amendment(amendment_amount_1)
    contract.add_amendment(amendment_amount_2)
    expect(contract.current.amount).to eq(5_000)
  end

  it 'apply 2 repetitions amendments to confirmed contract' do
    contract.confirm
    contract.add_amendment(amendment_repetitions_1)
    contract.add_amendment(amendment_repetitions_2)
    expect(contract.current.repetitions).to eq(20)
  end

  it 'apply 2 amendments and get info for the first' do
    contract.confirm
    contract.add_amendment(amendment_repetitions_1)
    contract.add_amendment(amendment_repetitions_2)
    expect(contract.at_amendment(1).repetitions).to eq(10)
  end

  it 'apply 2 different amendments' do
    contract.confirm
    contract.add_amendment(amendment_repetitions_1)
    contract.add_amendment(amendment_amount_1)
    expect(contract.current.repetitions).to eq(10)
    expect(contract.current.amount).to eq(20_000)
  end

  it 'original stays the same when applying amendments' do
    contract.confirm
    contract.add_amendment(amendment_repetitions_1)
    contract.add_amendment(amendment_repetitions_2)
    expect(contract.original.repetitions).to eq(3)
  end
end
