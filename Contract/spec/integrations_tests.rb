require 'rspec'
require 'date'
require_relative '../models/contract'
require_relative '../models/errors'

describe 'Integration tests' do
  let(:date) { DateTime.parse('2019-01-01') }
  let(:signals) { %w[volver canal13] }
  let(:contract_info) { ContractInfo.new(10_000, ['Volver al futuro'], 1, 1, signals) }
  let(:contract) { Contract.new(date, 'artear', contract_info_1) }
  let(:amendment_repetitions) { AmendmentRepetitions.new(10) }
  let(:amendment_amount) { AmendmentAmount.new(20_000) }

  it 'unconfirmed contract can be modified' do
    new_info = contract.current
    new_info.amount = 20_000
    contract.modify_info(new_info)
    expect(contract.current.amount).to eq(20_000)
  end

  it 'confirmed contract can not be modified' do
    contract.confirm
    new_info = contract.current
    new_info.amount = 20_000
    expect { contract.modify_info(new_info) }.to raise_error(InvalidContractAction)
  end

  it 'unconfirmed contract can not be amended' do
    expect { contract.add_amendment(amendment_amount) }.to raise_error(InvalidContractAction)
  end

  it 'confirmed contract can be amended' do
    contract.confirm
    contract.add_amendment(amendment_amount)
    expect(contract.current.amount).to eq(20_000)
  end

  # rubocop:disable RSpec/ExampleLength, RSpec/MultipleExpectations
  it 'contract with different amendments' do
    contract.confirm
    contract.add_amendment(amendment_amount)
    contract.add_amendment(amendment_repetitions)
    expect(contract.current.amount).to eq(20_000)
    expect(contract.original.amount).to eq(10_000)
    expect(contract.current.repetitions).to eq(10)
    expect(contract.at_amendment(1).repetitions).to eq(1)
    expect(contract.at_amendment(2).repetitions).to eq(10)
    expect(contract.original.repetitions).to eq(1)
    expect(contract.current.frequency).to eq(1)
    expect(contract.original.frequency).to eq(1)
  end
  # rubocop:enable RSpec/ExampleLength, RSpec/MultipleExpectations
end
