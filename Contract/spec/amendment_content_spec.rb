require 'rspec'
require_relative '../models/amendments/amendment_content'
require_relative '../models/contract_info'
require_relative '../models/errors'

describe 'AmendmentContent' do
  let(:contract_info) { ContractInfo.new(10_000, ['Harry Potter'], 3, 7, ['channel1']) }
  let(:amendment) { AmendmentContent.new(['Hulk']) }

  it 'applies correctly to contract info' do
    expect(amendment.apply(contract_info).content).to eq(['Hulk'])
  end

  it 'it is invalid if content is empty' do
    expect { AmendmentContent.new([]) }.to raise_error(InvalidAmendment)
  end
end
