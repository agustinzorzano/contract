require 'rspec'
require_relative '../models/amendments/amendment_amount'
require_relative '../models/contract_info'
require_relative '../models/errors'

describe 'AmendmentAmount' do
  let(:contract_info) { ContractInfo.new(10_000, ['Harry Potter'], 3, 7, ['channel1']) }
  let(:amendment) { AmendmentAmount.new(20_000) }

  it 'applies correctly to contract info' do
    expect(amendment.apply(contract_info).amount).to eq(20_000)
  end

  it 'it is invalid if amount is negative' do
    expect { AmendmentAmount.new(-20_000) }.to raise_error(InvalidAmendment)
  end
end
